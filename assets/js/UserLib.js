$(function() {

    //Wait for Pinegrow to wake-up
    $("body").one("pinegrow-ready", function(e, pinegrow) {

        //Create new Pinegrow framework object
        var f = new PgFramework("UserLib", "UserLib");

        //This will prevent activating multiple versions of this framework being loaded
        f.type = "UserLib";
        f.allow_single_type = true;
        f.user_lib = true

        var comp_comp1 = new PgComponentType('comp1', 'Site / Navbar');
        comp_comp1.code = '<nav class="navbar navbar-expand-lg navbar-light bg-light main-nav"> \
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler27" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation"> \
        <span class="navbar-toggler-icon"></span> \
    </button>     \
    <div class="collapse navbar-collapse" id="navbarToggler27"> \
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0"> \
            <li class="nav-item active">\
                <a class="nav-link" href="#">Pendants</a> \
            </li>             \
            <li class="nav-item"> \
                <a class="nav-link" href="#">Beads</a> \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link" href="#">Rings</a> \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink28" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Earrings</a> \
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink28"> \
                    <a class="dropdown-item" href="#">Art Earrings</a>\
                    <a class="dropdown-item" href="#">Beaded Earrings</a> \
                </div>                 \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link" href="#">Bracelets</a> \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link" href="#">Sets</a> \
            </li>             \
        </ul>         \
        <form class="form-inline my-2 my-lg-0"> \
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"> \
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>             \
        </form>         \
    </div>     \
</nav>';
        comp_comp1.parent_selector = null;
        f.addComponentType(comp_comp1);
        
        var comp_comp2 = new PgComponentType('comp2', 'Site Navbar');
        comp_comp2.code = '<nav class="navbar navbar-expand-lg navbar-light bg-light main-nav"> \
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler27" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation"> \
        <span class="navbar-toggler-icon"></span> \
    </button>     \
    <div class="collapse navbar-collapse" id="navbarToggler27"> \
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0"> \
            <li class="nav-item active">\
                <a class="nav-link" href="#">Pendants</a> \
            </li>             \
            <li class="nav-item"> \
                <a class="nav-link" href="#">Beads</a> \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link" href="#">Rings</a> \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink28" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Earrings</a> \
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink28"> \
                    <a class="dropdown-item" href="#">Art Earrings</a>\
                    <a class="dropdown-item" href="#">Beaded Earrings</a> \
                </div>                 \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link" href="#">Bracelets</a> \
            </li>\
            <li class="nav-item"> \
                <a class="nav-link" href="#">Sets</a> \
            </li>             \
        </ul>         \
        <form class="form-inline my-2 my-lg-0"> \
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"> \
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>             \
        </form>         \
    </div>     \
</nav>';
        comp_comp2.parent_selector = null;
        f.addComponentType(comp_comp2);
        
        //Tell Pinegrow about the framework
        pinegrow.addFramework(f);
            
        var section = new PgFrameworkLibSection("UserLib_lib", "Components");
        //Pass components in array
        section.setComponentTypes([comp_comp1, comp_comp2]);

        f.addLibSection(section);
   });
});